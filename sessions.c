/* Examine all the clients attached to our screen and find the one
 * with the least idle time. */

/* Inconsistent results may appear in case of nested screens on the
 * same system. */

#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <alloca.h>

#include "screenenv.h"

struct procinfo {
	pid_t pid, ppid;
	char *name;
	char buf_[1024];
};

/* Load procinfo for given process. */
static void get_procinfo(pid_t pid, struct procinfo *pi)
{
	pi->pid = pid;
	pi->name = NULL;

	char statfname[64];
	snprintf(statfname, sizeof(statfname), "/proc/%d/stat", pid);
	int fd = open(statfname, O_RDONLY);
	if (fd < 0) return;

	ssize_t r = read(fd, pi->buf_, sizeof(pi->buf_) - 1);
	if (r <= 0) return;
	pi->buf_[r] = 0;
	close(fd);

	pi->name = strchr(pi->buf_, '(') + 1;
	char *nameend = strrchr(pi->buf_, ')');
	*nameend = 0;
	pi->ppid = atol(nameend + 4);
}

/* Find master screen process. */
static pid_t get_screen_pid(pid_t base_pid)
{
	struct procinfo pi = { .ppid = base_pid };
	do {
		get_procinfo(pi.ppid, &pi);
	} while (pi.name && strcmp(pi.name, "screen") && pi.ppid > 0);
	if (!pi.name || pi.ppid <= 0)
		return -1;
	return pi.pid;
}


/* Find pts of a process with the least idle time. */
static dev_t get_active_pts(pid_t pid)
{
	char fddname[64];
	snprintf(fddname, sizeof(fddname), "/proc/%d/fd", pid);
	DIR *fdd = opendir(fddname);
	if (!fdd) return 0;

	dev_t bestpts = 0;
	time_t bestatime = 0;

	struct dirent *de, *res;
	de = alloca(sizeof(*de) + 4096);
	while (1) {
		if (readdir_r(fdd, de, &res) != 0)
			return 0;
		if (!res)
			break;

		/* Pick files we can see and are pts devices */
		struct stat s;
		if (fstatat(dirfd(fdd), de->d_name, &s, 0) != 0)
			continue;
		if (major(s.st_rdev) != 136)
			continue;
		/* and choose the one with the least idletime. */
		if (s.st_atime < bestatime)
			continue;
		bestpts = s.st_rdev;
		bestatime = s.st_atime;
	}

	closedir(fdd);
	return bestatime > 0 ? bestpts : 0;
}


/* Scan all processes for a screen with the given pts as stdin. */
static pid_t get_client_by_pts(dev_t pts)
{
	DIR *procd = opendir("/proc");
	if (!procd) return 0;

	struct dirent *de, *res;
	de = alloca(sizeof(*de) + 4096);
	while (1) {
		if (readdir_r(procd, de, &res) != 0)
			return 0;
		if (!res)
			break;
		int namelen = strlen(de->d_name);

		/* Pick numerical directories */
		if (de->d_type != DT_DIR || !isdigit(de->d_name[0]))
			continue;

		/* Peek at fd 0 */
		char name[namelen + 7];
		stpcpy(stpcpy(name, de->d_name), "/fd/0");

		struct stat s;
		if (fstatat(dirfd(procd), name, &s, 0) != 0)
			continue;
		if (s.st_rdev != pts)
			continue;

		/* Is this screen? */
		pid_t pid = atol(de->d_name);
		struct procinfo pi;
		get_procinfo(pid, &pi);
		if (pi.name == NULL || strcmp(pi.name, "screen"))
			continue;

		closedir(procd);
		return pid;
	}

	closedir(procd);
	return -1;
}


/* Find the client screen process watching base_pid with the least
 * idle time. */
pid_t get_client_pid(pid_t base_pid)
{
	static pid_t screen_pid;
	if (screen_pid < 0) return -1;
	if (screen_pid == 0) {
		screen_pid = get_screen_pid(base_pid);
		if (screen_pid < 0) return -1;
	}
	dev_t client_pts = get_active_pts(screen_pid);
	if (client_pts == 0) return -1;
	return get_client_by_pts(client_pts);
}


#if 0
int main(void)
{
	printf("%d\n", get_client_pid(getpid()));
}
#endif
