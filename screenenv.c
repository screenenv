/* The customized getenv() function itself. */

/* We keep a cached environment for the latest client, with only
 * the interesting variables picked out. */
/* FIXME: We should keep the old clients' environments as well,
 * in case the app uses stale getenv() pointers! */

#define _GNU_SOURCE 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dlfcn.h>

#include "screenenv.h"

/* Just add more if you need. */
const static char *grabenv[] = {
	"DISPLAY",
	// "SSH_AUTH_SOCK", - enable if you use ssh agent forwarding rigoriously
	"SSH_CLIENT",
	"SSH_CONNECTION",
	"SSH_TTY",
	"XDG_SESSION_COOKIE",
	NULL
};


/* getenv() upcall setup. */

static char *(*up_getenv)(const char *);
static int in_screen = 0;

static void __attribute__((constructor))
up_getenv_setup(void)
{
	/* XXX: We do this here instead of lazily on first call, since
	 * there is a possibility of dlsym()-malloc() race when called
	 * within a getenv() call. */
	up_getenv = dlsym(RTLD_NEXT, "getenv");

	char *term = up_getenv("TERM");
	in_screen = term && !strcmp(term, "screen");
}


/* The cached environment. */

struct env {
	pid_t pid;
	char *vars[sizeof(grabenv) / sizeof(*grabenv)];
	char buf_[4096];
};

static struct env env0;


/* Parse environment in /proc/.../environ and save it in *env. */

int loadenv(pid_t pid, struct env *env)
{
	env->pid = pid;

	/* XXX: All the code below is utterly stupid since we should've just
	 * mmap()'d the file and parse it in place. */

	char envname[128];
	snprintf(envname, 128, "/proc/%d/environ", pid);
	int fd = open(envname, O_RDONLY);
	if (fd < 0) return -1;

	char *envp = env->buf_;

	char buf[8192], *bufp;
	size_t bl = 0;

	/* What do we do in this read() iteration; we keep enough context
	 * necessary in the buffer from the previous iteration; if the
	 * variable is not interesting or is too long for our taste, we
	 * just skip to next one. */
	enum { VARNAME_LOAD, VALUE_LOAD, VAR_SKIP } state = VARNAME_LOAD;
	int var;

	while (1) {
		int l;
next_data:
		l = read(fd, buf + bl, sizeof(buf) - bl);
		if (l < 0) {
			close(fd);
			return -1;
		}
		bl += l;
		if (bl <= 0) break;
		bufp = buf;

		while (bl > 0) {
			char *tok;
			switch (state) {
				case VARNAME_LOAD:
					tok = memchr(bufp, '=', bl);
					if (!tok) {
						if (buf == bufp) {
							state = VAR_SKIP;
							bl = 0;
						} else {
							memmove(buf, bufp, bl);
						}
						goto next_data;
					}
					*tok++ = 0;
					for (var = 0; grabenv[var]; var++)
						if (!strcmp(bufp, grabenv[var]))
							break;
					bl -= tok - bufp; bufp = tok;
					if (grabenv[var]) {
						state = VALUE_LOAD;
					} else {
						state = VAR_SKIP;
					}
					break;
				case VAR_SKIP:
					tok = memchr(bufp, 0, bl);
					if (!tok) {
						bl = 0;
						goto next_data;
					}
					tok++;
					bl -= tok - bufp; bufp = tok;
					state = VARNAME_LOAD;
					break;
				case VALUE_LOAD:
					tok = memchr(bufp, 0, bl);
					if (!tok) {
						if (buf == bufp) {
							state = VAR_SKIP;
							bl = 0;
						} else {
							memmove(buf, bufp, bl);
						}
						goto next_data;
					}
					tok++;
					int vallen = tok - bufp;
					if (sizeof(env->buf_) - (envp - env->buf_) < vallen) {
						/* Environment space full. */
						close(fd);
						return -1;
					}
					memcpy(envp, bufp, vallen);
					// printf("[%d] `%s'\n", var, envp);
					env->vars[var] = envp; envp += vallen;
					bl -= tok - bufp; bufp = tok;
					state = VARNAME_LOAD;
					break;
			}
		}
	}

	close(fd);
	return 0;
}


/* The getenv() wrapper itself! */

__attribute__((visibility("default"))) char *
getenv(const char *env)
{
	/* We may be called by a crazy
	 * 	up_getenv_setup() -> dlsym() -> calloc() -> getenv()
	 * sequence. */
	if (__builtin_expect(up_getenv == NULL, 0))
		return ""; /* XXX */

	/* The fast way out, not to hog non-screen processes. */
	if (__builtin_expect(!in_screen, 1))
		return up_getenv(env);

	/* TERM=screen - so probably we will get client pid? */
	pid_t pid = get_client_pid(getpid());
	if (__builtin_expect(pid <= 0, 0))
		return up_getenv(env);

	/* Do we look for a hijacked variable? */
	int var;
	for (var = 0; grabenv[var]; var++)
		if (__builtin_expect(!strcmp(env, grabenv[var]), 0))
			break;
	if (__builtin_expect(!grabenv[var], 1))
		return up_getenv(env);

	/* Should we reload the environment? */
	if (env0.pid != pid) {
		memset(&env0, 0, sizeof(env0));
		if (loadenv(pid, &env0) < 0)
			return up_getenv(env);
	}

	return env0.vars[var];
}

#if 0
void putsn(char *str)
{
	puts(str ? str : "(null)");
}

int main(void)
{
	putsn(getenv("USER"));
	putsn(getenv("DISPLAY"));
	putsn(getenv("TERM"));
	putsn(getenv("XDG_SESSION_COOKIE"));
	return 0;
}
#endif
