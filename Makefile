CFLAGS=-Wall -O3

screenenv.so: screenenv.c sessions.c
	$(CC) $(CFLAGS) $(LDFLAGS) -shared -fPIC -fvisibility=internal -o $@ $^ -ldl

screenenv.c sessions.c: screenenv.h

clean:
	rm -f screenenv.so
